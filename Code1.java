import java.util.*;

class Solution {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> numIndexMap = new HashMap<>();

        for(int i = 0; i < nums.length; i++){
            int fnum = target - nums[i];
            if(numIndexMap.containsKey(fnum)){
                return new int[]{numIndexMap.get(fnum), i};
            }
            numIndexMap.put(nums[i], i);
        }
        
        throw new IllegalArgumentException("No solution found");
    }

    public static void main(String[] s){
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the number of elements in the array: ");
        int n = sc.nextInt();

        int nums[] = new int[n];
        System.out.println("Enter the elements of the array: ");
        for(int i = 0; i < n; i++){
            nums[i] = sc.nextInt();
        }

        System.out.println("Enter the target sum: ");
        int target = sc.nextInt();

        Solution solution = new Solution();
        try{
            int result[] = solution.twoSum(nums, target);
            System.out.println("Output: [" + result[0] + ", " + result[1] + "]");
        } catch (IllegalArgumentException e){
            //System.out.println("No solution found");
        }
    }
}
