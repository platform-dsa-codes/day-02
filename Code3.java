import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	public static void main(String[] args) throws IOException
	{
	        BufferedReader br =
            new BufferedReader(new InputStreamReader(System.in));
        int t =
            Integer.parseInt(br.readLine().trim());
        while(t-->0)
        {
            long n = Long.parseLong(br.readLine().trim());
            long a[] = new long[(int)(n)];
            String inputLine[] = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Long.parseLong(inputLine[i]);
            }
            
            Compute obj = new Compute();
            long[] product = obj.minAnd2ndMin(a, n); 
            if(product[0]==-1)
                System.out.println(product[0]);
            else
                System.out.println(product[0]+" "+product[1]);
            
        }
	}
}


class Compute 
{
    public long[] minAnd2ndMin(long a[], long n)  
    {
        long[] _final={-1,-1};
        long fs=Long.MAX_VALUE;
        long ss=Long.MAX_VALUE;
        long[] ak={-1};
        for (int i=0;i<a.length;i++){
            if (fs>a[i]) {
                fs=a[i];
            }
            _final[0]=fs;
        }
        for (int i=0;i<a.length;i++){
            if (ss>a[i]&&a[i]!=fs){
                ss=a[i];
            }
            _final[1]=ss;
        }
        if (ss == Long.MAX_VALUE) {
            return ak;
        }
        else
            return _final;

    }
}
