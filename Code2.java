import java.util.*;

class Solution {
    public int removeElement(int[] nums, int val) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[index] = nums[i];
                index++;
            }
        }
        return index;
    }
    
    public static void main(String[] args) {

        int[] nums = {3, 2, 2, 3};
        int val = 3;
        Solution solution = new Solution();
        int result = solution.removeElement(nums, val);
        System.out.println("Output: " + result);
        System.out.println("Modified Array: " + Arrays.toString(Arrays.copyOf(nums, result)));
        System.out.println();
    }
}
